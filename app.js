/*------------------For I----------------------------------*/
const plotMatchesPlayedPerYear = () => {
    return fetch('getNoOfMatchesPlayed.json').then((response) => {
        return response.json();
    }).then((mydata) => {
        let dataArr = [];
        for (let key in mydata) {
            dataArr.push({
                'name': key,
                'y': mydata[key]
            });
        }
        console.log(dataArr);
        createChartforI(dataArr);
    });
}
// plotMatchesPlayedPerYear();

// Create the chart for Number of Matches Played Per Year
const createChartforI = (data) => {
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'IPL - Number of Matches Played per year'
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Years'
            }
        },
        yAxis: {
            title: {
                text: 'Number of Matches Played'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:10px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
        },

        "series": [{
            "name": "Matches",
            "colorByPoint": true,
            "data": data
        }]
    });
}


/*------------------For II----------------------------------*/
const plotMatchesWonPerTeamPerYear = () => {
    return fetch('getNoOfMatchesWonPerTeamPerYear.json').then((response) => {
        return response.json();
    }).then((mydata) => {
        let dataArr = [];
        let years = [];
        let teams = [];
        for(let year in mydata) {
            years.push(year);
            // let wonPerYear = [];
            
            for(let team in mydata[year]) {
                if(teams.indexOf(team) < 0){
                    teams.push(team);
                    // teams.map((value) => {
                    //     if(value === team) {
                    //         dataArr.push({name : team})
                    //     }
                    // })
                }
                // wonPerYear.push(mydata[year][team])
                // dataArr.push({name : team})
            }
            // console.log(wonPerYear);

        }
        // console.log(dataArr);
        // dataArr.map((value) => {
        //     let won = [];
        //     if(teams.indexOf(value['name']) >= 0) {
        //         for(let year in mydata) {
        //             // console.log(mydata[year][value['name']]);
        //             won.push(mydata[year][value['name']]);
        //         }
        //     } else {
        //         won.push(0);
        //     }
        //     // for(let year in mydata) {
        //     //     // console.log(value['name']);
        //     //     if(teams.indexOf(value['name']) >= 0) {
        //     //         // console.log(year[value['name']])
        //     //         won.push(year[value['name']]);
        //     //     } else {
        //     //         won.push(0);
        //     //     }
        //     // }
        //     // console.log(won);
        //     value['data'] = won;
        // })

        teams.forEach((team) => {
            let obj = {name: team, data: []};
            for(let year in mydata) {
                if(mydata[year].hasOwnProperty(team)) {
                    obj['data'].push(mydata[year][team]);
                } else {
                    obj['data'].push(0);
                }
            }
            dataArr.push(obj);
        })

        dataArr.forEach((value) => {
            if(value['name'] == ''){
                dataArr.splice(dataArr.indexOf(value), 1);
            }
        })
        // console.log(teams);
        console.log(dataArr);
        // console.log(years);
        createChartforII(dataArr, years);
    })
}

const createChartforII = (data, years) => {
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Matches Won per Team per Year'
    },
    xAxis: {
        categories: years
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Matches Won'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: data
});
}




/*------------------For III----------------------------------*/
const plotExtraRunsPerTeamForYear = () => {
    return fetch('getExtraRunsPerTeamForYear.json').then((response) => {
        return response.json();
    }).then((mydata) => {
        let dataArr = [];
        for (let key in mydata) {
            dataArr.push({
                'name': key,
                'y': mydata[key]
            });
        }
        console.log(dataArr);
        createChartForIII(dataArr);
    });
}
// plotExtraRunsPerTeamForYear();

// Create the chart for Extra Runs Per Team for Year
const createChartForIII = (data) => {
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'IPL - Extra Runs Per Team For year 2016'
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Teams'
            }
        },
        yAxis: {
            title: {
                text: 'Extra Runs Given'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:10px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
        },

        "series": [{
            "name": "Extra Runs",
            "colorByPoint": true,
            "data": data
        }]
    });
}






/*------------------For IV----------------------------------*/
const plotEconomicalBowlersForYear = () => {
    return fetch('getEconomicalBowlersForYear.json').then((response) => {
        return response.json();
    }).then((mydata) => {
        let dataArr = [];
        for (let key in mydata) {
            dataArr.push({
                'name': key,
                'y': mydata[key]
            });
        }
        console.log(dataArr);
        createChartforIV(dataArr);
    });
}
// plotEconomicalBowlersForYear();

// Create the chart for Extra Runs Per Team for Year
const createChartforIV = (data) => {
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'IPL - Economical Bowlers For year 2015'
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Bowlers'
            }
        },
        yAxis: {
            title: {
                text: 'Average Runs per Over'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:10px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
        },

        "series": [{
            "name": "Average Runs given per Over",
            "colorByPoint": true,
            "data": data
        }]
    });
}


